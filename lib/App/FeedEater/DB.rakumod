use v6;

unit class App::FeedEater::DB;
use DB::SQLite;

has IO::Path $.data-dir = .new(%*ENV<XDG_DATA_HOME> ~ '/feedeater');
has IO::Path $.db-file = .new($!data-dir.child('feeds.db'));
has DB::SQLite $.db;

method init {
    mkdir $.data-dir unless $.data-dir.IO.e;
    say 'data dir initialised: ' ~ $.data-dir.path;

    my $.db = DB::SQLite.new(filename => $.db-file.Str),;

    $.db.execute('create table if not exists feed (url text primary key, title text)');

    return;
}

method add-feed(Str:D $url, Str:D $title) {
    return $.db.query('insert or ignore into feed (url, title) values (?, ?)', $url, $title);
}
