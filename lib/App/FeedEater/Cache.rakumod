use v6;

unit class App::FeedEater::Cache;

use Digest::SHA1::Native;

has IO::Path $.cache-dir = .new(%*ENV<XDG_CACHE_HOME> ~ '/feedeater');

method init {
    mkdir $.cache-dir unless $.cache-dir.IO.e;
    say 'cache initialised: ' ~ $.cache-dir.path;
}

method get(Str:D $url) {
    my $sha = sha1-hex $url;
    my $file = $.cache-dir.child($sha);

    if ($file.IO.e) {
        return $file.slurp;
    }

    return;
}

method set(Str:D $url, Str:D $opml) {
    my $sha = sha1-hex $url;
    my $file = $.cache-dir.child($sha);

    return $file.spurt($opml);
}