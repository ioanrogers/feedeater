unit class App::FeedEater;

use HTTP::UserAgent;
use XML::XPath;
use App::FeedEater::Cache;
use App::FeedEater::DB;

has App::FeedEater::Cache $!cache = .new;
has App::FeedEater::DB $!db = .new;
has $.fetcher;

has Bool $.verbose       = False;
has HTTP::UserAgent $!ua;

method add_feed(@feeds) {
    $!cache.init;
    $!db.init;

    my $ua = HTTP::UserAgent.new;
    for @feeds -> $feed {
        say "Fetching [$feed]...";

        my $opml = $!cache.get($feed);
        if !$opml {
            say "$feed not cached, fetching...";
            my $response = $ua.get($feed);

            if !$response.is-success {
                note "Failed to retrived feed from $feed: $response.status-line";
                next;
            }
            $opml = $response.content;
            $!cache.set($feed, $opml);
        }

        my $xpath  = XML::XPath.new(xml => $opml);

        # some sites don't use `type` attr /opml/body/outline[@type="rss"]'
        my @result = $xpath.find('/opml/body/outline[@xmlUrl]', :to-list);

        say @result.elems;
        for @result -> $node {
            say $node.attribs<text>;
            say $node.attribs<xmlUrl>;
            $!db.add-feed($node.attribs<xmlUrl>, $node.attribs<text>)
        }
    }
}
